CREATE TABLE funcionarios (
                           ci_funcionario BIGINT PRIMARY KEY,
                           nome VARCHAR(256) NOT NULL ,
                           cpf BIGINT NOT NULL,
                           data_nascimento DATE NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO funcionarios (ci_funcionario, nome, cpf, data_nascimento) values (19981010, 'João da Silva', 28352113062, '1998-10-10');