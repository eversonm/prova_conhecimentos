package com.funcionarios.funcionario.servico;

import com.funcionarios.funcionario.entidades.Funcionario;
import com.funcionarios.funcionario.repositorio.FuncionarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FuncionarioServico {

    @Autowired
    private FuncionarioRepositorio funcionarioRepositorio;

    public List<Funcionario> listarTodos(){
        return funcionarioRepositorio.findAll();
    }

    public Optional<Funcionario> listarPorCpf(Long cpf){
        return Optional.ofNullable(funcionarioRepositorio.findByCpf(cpf));
    }


    public Funcionario inserirFuncionario(Funcionario funcionario){
        return funcionarioRepositorio.save(funcionario);
    }
}
