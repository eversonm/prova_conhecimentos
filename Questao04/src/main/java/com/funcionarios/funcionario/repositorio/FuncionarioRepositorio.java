package com.funcionarios.funcionario.repositorio;

import com.funcionarios.funcionario.entidades.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FuncionarioRepositorio extends JpaRepository<Funcionario, Long> {
    Funcionario findByCpf(Long cpf);
}
