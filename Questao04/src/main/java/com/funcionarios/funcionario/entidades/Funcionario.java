package com.funcionarios.funcionario.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="funcionario")
public class Funcionario {
    @Id
    private Long ci_funcionario;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private Long cpf;

    @Column(name="data_nascimento")
    private String data_nascimento;

    public Long getCi_funcionario() {
        return ci_funcionario;
    }

    public void setCi_funcionario(Long ci_funcionario) {
        this.ci_funcionario = ci_funcionario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(String data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Funcionario that = (Funcionario) o;
        return ci_funcionario.equals(that.ci_funcionario) && nome.equals(that.nome) && cpf.equals(that.cpf) && data_nascimento.equals(that.data_nascimento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ci_funcionario, nome, cpf, data_nascimento);
    }
}
