package com.funcionarios.funcionario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"com.funcionarios.funcionario.entidades"})
@EnableJpaRepositories(basePackages = {"com.funcionarios.funcionario.repositorio"})
@ComponentScan(basePackages = {"com.funcionarios.funcionario.servico", "com.funcionarios.funcionario.controlador"})
@SpringBootApplication
public class FuncionarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuncionarioApplication.class, args);
	}

}
