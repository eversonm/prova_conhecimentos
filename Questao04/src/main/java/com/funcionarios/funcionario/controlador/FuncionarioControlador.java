package com.funcionarios.funcionario.controlador;

import com.funcionarios.funcionario.entidades.Funcionario;
import com.funcionarios.funcionario.servico.FuncionarioServico;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Api(tags = "Funcionario")
@RestController
@RequestMapping("/funcionario")
public class FuncionarioControlador {

    @Autowired
    private FuncionarioServico funcionarioServico;

    @ApiOperation(value = "Listar todos os funcionários")
    @GetMapping
    public List<Funcionario> listarTodos(){
        return funcionarioServico.listarTodos();
    }

    @ApiOperation(value = "Listar funcionário por CPF")
    @GetMapping("/{cpf}")
    public ResponseEntity<Optional<Funcionario>> listarPorCpf(@PathVariable Long cpf) {
        Optional<Funcionario> funcionario = funcionarioServico.listarPorCpf(cpf);
        return funcionario.isPresent() ? ResponseEntity.ok(funcionario) : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Criar novo funcionário")
    @PostMapping
    public ResponseEntity<Funcionario> inserirFuncionario(@RequestBody Funcionario funcionario){
        Funcionario funcionarioSalvo = funcionarioServico.inserirFuncionario(funcionario);
        return ResponseEntity.status(HttpStatus.CREATED).body(funcionarioSalvo);
    }
}
