def main():
    """Ideia: Reverter a ordem da string e comparar com a string original"""
    print("Verificador de palindromos")
    string_pal = str(input("Entre com uma palavra: "))

    if string_pal == string_pal[::-1]:
        print("Essa palavra '" + string_pal + "' é um palíndromo!")
    else:
        print("Essa palavra '" + string_pal + "' não é um palíndromo!")

if __name__ == "__main__":
    main()
