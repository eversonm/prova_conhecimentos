# Sistema de gerenciamento de cursos

![Alt text](sistema-cursos.png?raw=true "Questao6") 

### Aplicação construída usando React, Redux e Hooks
</br>

### Funções
*É possível adicionar um curso novo, editar um curso existente e remover um curso existente.*

### `npm install`
Para instalar as dependências da aplicação.

### `npm start`

Executa a aplicação em modo desenvolvedor.\
Abra [http://localhost:3000](http://localhost:3000) para visualizar no seu navegador.

### `npm run build`

Cria o aplicativo para produção na pasta `build`.
