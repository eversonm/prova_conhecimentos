import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { courseActions } from "../store/course";
import Course from "./Course";
import classes from "./CourseList.module.css";

const CourseList = (props) => {
  const dispatch = useDispatch();
  const [enteredCourse, setNewEnteredCourse] = useState("");

  const addCourse = (event) => {
    event.preventDefault();
    const id = "id" + Math.random().toString(16).slice(2);
    dispatch(courseActions.addCourse({ id: id, title: enteredCourse }));
  };

  const onChangeHandler = (event) => {
    setNewEnteredCourse(event.target.value);
  };

  const courses = useSelector((state) => state.course.course);

  if (courses.length === 0) {
    return (
      <div className="center">
        <h2>Nenhum curso encontrado!</h2>
      </div>
    );
  }

  return (
    <div className={classes.modal}>
      <div>
        <ul className={classes["ul"]}>
          {courses.map((c) => (
            <Course key={c.id} item={{ id: c.id, title: c.title }} />
          ))}
        </ul>
      </div>
      <div className={classes["div"]}>
        <input
          className={classes["input"]}
          type="text"
          value={enteredCourse}
          onChange={onChangeHandler}
        />
        <button onClick={addCourse}>
          Adicionar curso
        </button>
      </div>
    </div>
  );
};

export default CourseList;
