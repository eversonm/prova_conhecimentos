import React, { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { courseActions } from "../store/course";
import "./Course.module.css";

const Course = (props) => {
  const dispatch = useDispatch();
  const { id, title } = props.item;
  const [updatedCourse, setUpdatedCourse] = useState("");
  const [showInput, setShowInput] = useState(false);
  const inputRef = useRef("");

  const handleClick = () => {
    // inputRef.current.click();
    setShowInput(!showInput);
  };

  const handleInputUpdate = (event) => {
    setUpdatedCourse(event.target.value);
    // event.target.value = null;
  };

  const updateHandle = (event) => {
    event.preventDefault();
    dispatch(courseActions.editCourse({ id: id, title: updatedCourse }));
    setShowInput(!showInput);
    inputRef.current.value = "";
  };

  return (
    <li key={id}>
      {title}
      {showInput && (
        <>
          <input
            ref={inputRef}
            type="text"
            onChange={handleInputUpdate}
          />
          <button type="button" onClick={updateHandle}>
            Update
          </button>
        </>
      )}
      {!showInput && (
        <button type="button" onClick={handleClick}>
          Editar
        </button>
      )}
      <button
        type="button"
        onClick={() => dispatch(courseActions.removeCourse(id))}
      >
        Remover
      </button>
    </li>
  );
};

export default Course;
