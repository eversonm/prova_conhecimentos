import CourseList from "./components/CourseList";
import { Provider } from "react-redux";
import store from "./store/index";
import Header from "./components/Header";

function App() {
  return (
    <Provider store={store}>
      <Header />
      <CourseList />
    </Provider>
  );
}

export default App;
