import { configureStore } from "@reduxjs/toolkit";
import courseReducer from "./course";

const store = configureStore({ reducer: {course: courseReducer} });

export default store;
