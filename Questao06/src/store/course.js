import { createSlice } from "@reduxjs/toolkit";

const INITIAL_STATE = {
  course: [
    {
      id: "c1",
      title: "React - The Complete Guide (incl Hooks, React Router, Redux)",
    },
    {
      id: "c2",
      title: "API Rest com Java e Spring Boot do iniciante ao especialista",
    },
  ],
};

const courseSlice = createSlice({
  name: "course",
  initialState: INITIAL_STATE,
  reducers: {
    addCourse(state, action) {
      const newCourse = action.payload;
      const existingItem = state.course.find((c) => c.id === newCourse.id);
      if (!existingItem) {
        state.course.push({
          id: newCourse.id,
          title: newCourse.title,
        });
      }
    },
    removeCourse(state, action) {
      const id = action.payload;
      const existingItem = state.course.find((c) => c.id === id);
      if (existingItem) {
        state.course = state.course.filter((item) => item.id !== id);
      }
    },
    editCourse(state, action) {
      const item = action.payload;
      const existingItem = state.course.findIndex((c) => c.id === item.id);
      state.course[existingItem].title = item.title;
      // state.course[existingItem].titile = {
      //   id: item.id,
      //   title: item.title,
      // // };
      // console.log(item.id);
    },
  },
});

export const courseActions = courseSlice.actions;
export default courseSlice.reducer;
