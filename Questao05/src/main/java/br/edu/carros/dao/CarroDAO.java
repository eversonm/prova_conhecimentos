package br.edu.carros.dao;

import br.edu.carros.entidade.Carro;
import br.edu.carros.util.FabricaConexao;
import br.edu.carros.util.exception.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bolsa
 */
public class CarroDAO implements CrudDAO<Carro> {

    @Override
    public void salvar(Carro carro) throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps;
            if (carro.getId() == null) {
                ps = conexao.prepareStatement("INSERT INTO carro (modelo, fabricante, cor, ano) values (?, ?, ?, ?)");

            } else {
                ps = conexao.prepareStatement("UPDATE carro set modelo=?, fabricante=?, cor=?, ano=? WHERE id=?");
                ps.setInt(5, carro.getId());
            }
            ps.setString(1, carro.getModelo());
            ps.setString(2, carro.getFabricante());
            ps.setString(3, carro.getCor());
            //java.util.Date utilDate = new java.util.Date(carro.getAno());
            //System.out.println(carro.getAno());
            // 
            ps.setDate(4, new Date(carro.getAno().getTime()));
            //System.out.println(utilDate);
            ps.execute();
            FabricaConexao.fecharConexao();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao tentar salvar!", ex);
        }
    }

    @Override
    public void apagar(Carro carro) throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("DELETE FROM carro WHERE id=?");
            ps.setInt(1, carro.getId());
            ps.execute();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao apagar o carro!", ex);
        }
    }

    @Override
    public List<Carro> buscar() throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("SELECT * FROM carro");
            ResultSet resultSet = ps.executeQuery();
            List<Carro> carros = new ArrayList<>();
            while (resultSet.next()) {
                Carro carro = new Carro();
                carro.setId(resultSet.getInt("id"));
                carro.setModelo(resultSet.getString("modelo"));
                carro.setFabricante(resultSet.getString("fabricante"));
                carro.setCor(resultSet.getString("cor"));
                carro.setAno(resultSet.getDate("ano"));
                carros.add(carro);
            }
            FabricaConexao.fecharConexao();
            return carros;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar os carros!", ex);
        }
    }
}
