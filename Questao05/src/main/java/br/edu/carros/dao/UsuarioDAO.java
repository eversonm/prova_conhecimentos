/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.carros.dao;

import br.edu.carros.entidade.Carro;
import br.edu.carros.entidade.Usuario;
import br.edu.carros.util.FabricaConexao;
import br.edu.carros.util.exception.ErroSistema;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bolsa
 */
public class UsuarioDAO implements CrudDAO<Usuario>{

    @Override
    public void salvar(Usuario usuario) throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps;
            if (usuario.getId() == null) {
                ps = conexao.prepareStatement("INSERT INTO usuario (login, senha) values (?, ?)");

            } else {
                ps = conexao.prepareStatement("UPDATE usuario set login=?, senha=? WHERE id=?");
                ps.setInt(3, usuario.getId());
            }
            ps.setString(1, usuario.getLogin());
            ps.setString(2, usuario.getSenha());
            //System.out.println(utilDate);
            ps.execute();
            FabricaConexao.fecharConexao();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao tentar salvar!", ex);
        }
    }

    @Override
    public void apagar(Usuario usuario) throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("DELETE FROM usuario WHERE id=?");
            ps.setInt(1, usuario.getId());
            ps.execute();
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao apagar o usuario!", ex);
        }
    }

    @Override
    public List<Usuario> buscar() throws ErroSistema {
        try {
            Connection conexao = FabricaConexao.getConexao();
            PreparedStatement ps = conexao.prepareStatement("SELECT * FROM usuario");
            ResultSet resultSet = ps.executeQuery();
            List<Usuario> usuarios = new ArrayList<>();
            while (resultSet.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(resultSet.getInt("id"));
                usuario.setLogin(resultSet.getString("login"));
                usuario.setSenha(resultSet.getString("senha"));
                usuarios.add(usuario);
            }
            FabricaConexao.fecharConexao();
            return usuarios;
        } catch (SQLException ex) {
            throw new ErroSistema("Erro ao buscar os usuarios!", ex);
        }
    }
    
}
