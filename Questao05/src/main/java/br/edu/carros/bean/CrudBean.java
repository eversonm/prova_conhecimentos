/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.carros.bean;

import br.edu.carros.dao.CrudDAO;
import br.edu.carros.util.exception.ErroSistema;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author bolsa
 */
public abstract class CrudBean<E, D extends CrudDAO> {
    private String estadoTela = "buscar"; //insere, edita, busca
    
    private E entidade;
    private List<E> entidades;
    
    public void novo(){
        entidade = criarNovaEntidade();
        mudarParaInsere();
    }
    
    public void salvar(){
        try {
            getDao().salvar(entidade);
            entidade = criarNovaEntidade();
            adicionarMensagem("Salvo com sucesso!", FacesMessage.SEVERITY_INFO);
            mudarParaBusca();
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }
    
    public void editar(E entidade){
        this.entidade = entidade;
        mudarParaEdita();
    }
    
    public void apagar(E entidade){
        try {
            getDao().apagar(entidade);
            entidades.remove(entidade);
            adicionarMensagem("Apagado com sucesso!", FacesMessage.SEVERITY_INFO);
            mudarParaBusca();
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
        
    }
    
    public void buscar() {
        if (isBusca() != false){
        } else {
            mudarParaBusca();
            return;
        }
        try {
            entidades = getDao().buscar();
            if (entidades == null || entidades.size() < 1){
                adicionarMensagem("Não temos nada cadastrado!", FacesMessage.SEVERITY_WARN);
            }
        } catch (ErroSistema ex) {
            Logger.getLogger(CrudBean.class.getName()).log(Level.SEVERE, null, ex);
            adicionarMensagem(ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
        
    }
    
    public void adicionarMensagem(String mensagem, FacesMessage.Severity tipoErro){
        FacesMessage fm = new FacesMessage(tipoErro, mensagem, null);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }
    
    // Getters and setters

    public E getEntidade() {
        return entidade;
    }

    public void setEntidade(E entidade) {
        this.entidade = entidade;
    }

    public List<E> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<E> entidades) {
        this.entidades = entidades;
    }
    
    
    // Responsavel por criar metodos nas classes bean
    public abstract D getDao();
    public abstract E criarNovaEntidade();
    
    // Metodos para controle da tela
    public boolean isInsere(){
        return "inserir".equals(estadoTela);
    }
    
    public boolean isEdita(){
        return "editar".equals(estadoTela);
    }
    
    public boolean isBusca(){
        return "buscar".equals(estadoTela);
    }
    
    public void mudarParaInsere(){
        estadoTela = "inserir";
    }
    
    public void mudarParaEdita(){
        estadoTela = "editar";
    }
    
    public void mudarParaBusca(){
        estadoTela = "buscar";
    }
}
