/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.carros.bean;

import br.edu.carros.entidade.Carro;
import br.edu.carros.dao.CarroDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author bolsa
 */
@ManagedBean
@SessionScoped
public class CarroBean extends CrudBean<Carro, CarroDAO>{
    
    private CarroDAO carroDAO;

    @Override
    public CarroDAO getDao() {
        if (carroDAO == null){
            carroDAO = new CarroDAO();
        }
        return carroDAO;
    }

    @Override
    public Carro criarNovaEntidade() {
        return new Carro();
    }
    
}
