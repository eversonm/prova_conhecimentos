-- a)
select *
from funcionario f
left join vinculo v
on f.ci_funcionario=v.cd_funcionario
where v.cd_funcionario is null;

-- b)
select v.matricula
from funcionario f inner join vinculo v
where (f.ci_funcionario, v.matricula, f.data_nascimento) in (
    select f.ci_funcionario, v.matricula, f.data_nascimento
    from funcionario f, vinculo v
    where f.ci_funcionario=v.cd_funcionario
    order by f.data_nascimento desc
)

-- c)
select f.nome, f.cpf
from funcionario f
where ci_funcionario in (
    select distinct(f.ci_funcionario)
    from funcionario f, vinculo v
    where f.ci_funcionario=v.cd_funcionario
    group by f.ci_funcionario
    having count(v.matricula) > 3
)

-- d)
select tv.nome, l.atividade, l.data_inicio, l.data_fim
from funcionario f, vinculo v, tipo_vinculo tv, lotacao l
where f.ci_funcionario=v.cd_funcionario
and v.ci_vinculo=l.cd_vinculo
and tv.ci_tipo_vinculo=v.cd_tipo_vinculo
and v.matricula=91471
and YEAR(data_inicio)='2022';
